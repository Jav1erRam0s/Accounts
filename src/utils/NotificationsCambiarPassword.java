package utils;

import java.io.IOException;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import presentacion.controlador.ControladorVentanaCambiarPassword;
import presentacion.controlador.ControladorVentanaLogin;

public class NotificationsCambiarPassword extends Thread {

	private ControladorVentanaCambiarPassword contro;
	private String type;
	private String msg;
	
	public NotificationsCambiarPassword(ControladorVentanaCambiarPassword contro, String type, String msg){

		this.contro = contro;
		this.type = type;
		this.msg = msg;
		
		// Estilizar Panel de Notificacion.
		if(this.type.equals("cambiado")){
			this.contro.hboxNotification.setStyle("-fx-border-radius: 5; -fx-background-color: #abebc6; -fx-background-radius: 8; -fx-border-color:  transparent;");
			Image img0 = new Image("resources/images/cambiado.png");
		    ImageView view0 = new ImageView(img0);
		    view0.setFitHeight(20);
		    view0.setFitWidth(20);
		    contro.lblNoticationImage.setGraphic(view0);
		    contro.lblNoticationMessage.setText(this.msg);
		}
		else if(this.type.equals("error")){
			this.contro.hboxNotification.setStyle("-fx-border-radius: 5; -fx-background-color: #f5b7b1; -fx-background-radius: 8; -fx-border-color:  transparent;");
			Image img0 = new Image("resources/images/error.png");
		    ImageView view0 = new ImageView(img0);
		    view0.setFitHeight(20);
		    view0.setFitWidth(20);
		    contro.lblNoticationImage.setGraphic(view0);
		    contro.lblNoticationMessage.setText(this.msg);
		}
	}
	
	@Override
	public void run() {
		this.deshabilitar();
		
		this.contro.hboxNotification.setVisible(true);
		this.contro.hboxNotification.setOpacity(1);
		this.esperar(2000);
		this.contro.hboxNotification.setOpacity(0.9);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.8);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.7);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.6);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.5);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.4);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.3);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.2);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.1);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0);
		this.contro.hboxNotification.setVisible(false);

		if(this.type.equals("cambiado"))
		{
			Stage stagePass = (Stage) this.contro.newPass.getScene().getWindow();
			Stage stageMain = (Stage) this.contro.contro.btnConfigurar.getScene().getWindow();
			
			Platform.runLater(()->{
				stagePass.close();
				stageMain.close(); 
				this.abrirLogin();
			});
		}
		else
		{
			this.habilitar();
		}
	}

	private void esperar(int milisegundos)
	{
		try { Thread.sleep(milisegundos); }
		catch ( InterruptedException ex ) { Thread.currentThread().interrupt(); }
	}
	
	public void deshabilitar(){
		this.contro.newPass.clear();
		this.contro.repeatPass.clear();

		this.contro.lblPortada.setDisable(true);
		this.contro.newPass.setDisable(true);
		this.contro.repeatPass.setDisable(true);
	}
	
	public void habilitar(){
		this.contro.lblPortada.setDisable(false);
		this.contro.newPass.setDisable(false);
		this.contro.repeatPass.setDisable(false);
	}
	
	public void abrirLogin(){
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/presentacion/vista/VentanaLogin.fxml"));
		try 
		{
			//hacemos una referencia para cargar al padre.
			Parent root = loader.load();
			
			Scene scene = new Scene(root, 250, 250);
			
			Stage stageLogin = new Stage();
			stageLogin.setTitle("Accounts");
			stageLogin.getIcons().add(new Image("/resources/images/accounts.png"));
			
			ControladorVentanaLogin contro = loader.getController();
			contro.initialize();

			stageLogin.initModality(Modality.APPLICATION_MODAL);
			stageLogin.setScene(scene);
			stageLogin.setResizable(false);
			stageLogin.show();
			
	        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
	        stageLogin.setX((screenBounds.getWidth() - stageLogin.getWidth()) / 2);
	        stageLogin.setY((screenBounds.getHeight() - stageLogin.getHeight()) / 2);
		} 
		catch (IOException e) 
		{ e.printStackTrace(); }
	}
	
}