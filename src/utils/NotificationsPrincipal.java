package utils;

import java.io.IOException;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import modelo.GestorCuentas;
import presentacion.controlador.ControladorVentanaLogin;
import presentacion.controlador.ControladorVentanaPrincipal;

public class NotificationsPrincipal extends Thread {

	private ControladorVentanaPrincipal contro;
	private String type;
	private String msg;
	
	public NotificationsPrincipal(ControladorVentanaPrincipal contro, String type, String msg){

		this.contro = contro;
		this.type = type;
		this.msg = msg;
		
		// Estilizar Panel de Notificacion.
		if(this.type.equals("agregado")){
			this.contro.hboxNotification.setStyle("-fx-border-radius: 5; -fx-background-color: #abebc6; -fx-background-radius: 8; -fx-border-color:  transparent;");
			Image img0 = new Image("resources/images/agregado.png");
		    ImageView view0 = new ImageView(img0);
		    view0.setFitHeight(25);
		    view0.setFitWidth(25);
		    contro.lblNoticationImage.setGraphic(view0);
		    contro.lblNoticationMessage.setText(this.msg);
		}
		else if(this.type.equals("editado")){
			this.contro.hboxNotification.setStyle("-fx-border-radius: 5; -fx-background-color: #aed6f1; -fx-background-radius: 8; -fx-border-color:  transparent;");
			Image img0 = new Image("resources/images/editado.png");
		    ImageView view0 = new ImageView(img0);
		    view0.setFitHeight(25);
		    view0.setFitWidth(25);
		    contro.lblNoticationImage.setGraphic(view0);
		    contro.lblNoticationMessage.setText(this.msg);
		}
		else if(this.type.equals("eliminado")){
			this.contro.hboxNotification.setStyle("-fx-border-radius: 5; -fx-background-color: #f5b7b1; -fx-background-radius: 8; -fx-border-color:  transparent;");
			Image img0 = new Image("resources/images/eliminado.png");
		    ImageView view0 = new ImageView(img0);
		    view0.setFitHeight(25);
		    view0.setFitWidth(25);
		    contro.lblNoticationImage.setGraphic(view0);
		    contro.lblNoticationMessage.setText(this.msg);
		}
		else if(this.type.equals("exportado")){
			this.contro.hboxNotification.setStyle("-fx-border-radius: 5; -fx-background-color: #ccd1d1; -fx-background-radius: 8; -fx-border-color:  transparent;");
			Image img0 = new Image("resources/images/exportado.png");
		    ImageView view0 = new ImageView(img0);
		    view0.setFitHeight(25);
		    view0.setFitWidth(25);
		    contro.lblNoticationImage.setGraphic(view0);
		    contro.lblNoticationMessage.setText(this.msg);
		}
		else if(this.type.equals("importado")){
			this.contro.hboxNotification.setStyle("-fx-border-radius: 5; -fx-background-color: #ccd1d1; -fx-background-radius: 8; -fx-border-color:  transparent;");
			Image img0 = new Image("resources/images/importado.png");
		    ImageView view0 = new ImageView(img0);
		    view0.setFitHeight(25);
		    view0.setFitWidth(25);
		    contro.lblNoticationImage.setGraphic(view0);
		    contro.lblNoticationMessage.setText(this.msg);
		}
		else if(this.type.equals("reportado")){
			this.contro.hboxNotification.setStyle("-fx-border-radius: 5; -fx-background-color: #f9e79f; -fx-background-radius: 8; -fx-border-color:  transparent;");
			Image img0 = new Image("resources/images/reportado.png");
		    ImageView view0 = new ImageView(img0);
		    view0.setFitHeight(25);
		    view0.setFitWidth(25);
		    contro.lblNoticationImage.setGraphic(view0);
		    contro.lblNoticationMessage.setText(this.msg);
		}
		else if(this.type.equals("error")){
			this.contro.hboxNotification.setStyle("-fx-border-radius: 5; -fx-background-color: #f5b7b1; -fx-background-radius: 8; -fx-border-color:  transparent;");
			Image img0 = new Image("resources/images/error.png");
		    ImageView view0 = new ImageView(img0);
		    view0.setFitHeight(25);
		    view0.setFitWidth(25);
		    contro.lblNoticationImage.setGraphic(view0);
		    contro.lblNoticationMessage.setText(this.msg);
		}
	}
	
	@Override
	public void run() {
		this.limpiarFiltroTabla();
		
		this.contro.hboxNotification.setVisible(true);
		this.contro.hboxNotification.setOpacity(1);
		this.esperar(2000);
		this.contro.hboxNotification.setOpacity(0.9);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.8);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.7);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.6);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.5);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.4);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.3);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.2);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.1);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0);
		this.contro.hboxNotification.setVisible(false);

		if(this.type.equals("importado")){
			Stage stage = (Stage) this.contro.btnConfigurar.getScene().getWindow();
			Platform.runLater(()->{ 
				stage.close();
				this.abrirLogin();
			});
		}
		else{			
			this.cerrar();
		}
		
	}
	
	private void esperar(int milisegundos)
	{
		try { Thread.sleep(milisegundos); }
		catch ( InterruptedException ex ) { Thread.currentThread().interrupt(); }
	}
	
	public void limpiarFiltroTabla()
	{
		this.contro.txtFiltro.setText("");
		Platform.runLater(()->{
			this.contro.cuentas.clear();
			this.contro.cuentas = FXCollections.observableArrayList( GestorCuentas.getInstance().readAll() );
			this.contro.filtrar(null); 
		});
	}
	
	public void cerrar()
	{
	    this.contro.hBoxBuscador.setStyle("-fx-border-radius: 5; -fx-border-width: 2; -fx-border-style:solid; -fx-border-color: #004d67; -fx-background-color: black; -fx-background-radius: 8");
	    this.contro.hBoxActions.setStyle("-fx-border-radius: 5; -fx-border-width: 2; -fx-border-style:solid; -fx-border-color: #4d5656; -fx-background-color: black; -fx-background-radius: 8");
		
		this.contro.btnAgregar.setDisable(false);
		this.contro.MenuButtonBD.setDisable(false);
		this.contro.btnDescargar.setDisable(false);
		this.contro.btnConfigurar.setDisable(false);
		this.contro.btnBuscador.setDisable(false);
		
		this.contro.txtFiltro.setDisable(false);

		Platform.runLater(()->{ this.contro.txtFiltro.requestFocus(); });
		
		this.contro.tblCuentas.setDisable(false);
	}

	public void abrirLogin(){
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/presentacion/vista/VentanaLogin.fxml"));
		try 
		{
			//hacemos una referencia para cargar al padre.
			Parent root = loader.load();
			
			Scene scene = new Scene(root, 250, 250);
			
			Stage stageLogin = new Stage();
			stageLogin.setTitle("Accounts");
			stageLogin.getIcons().add(new Image("/resources/images/accounts.png"));
			
			ControladorVentanaLogin contro = loader.getController();
			contro.initialize();

			stageLogin.initModality(Modality.APPLICATION_MODAL);
			stageLogin.setScene(scene);
			stageLogin.setResizable(false);
			stageLogin.show();
			
	        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
	        stageLogin.setX((screenBounds.getWidth() - stageLogin.getWidth()) / 2);
	        stageLogin.setY((screenBounds.getHeight() - stageLogin.getHeight()) / 2);
		} 
		catch (IOException e) 
		{ e.printStackTrace(); }
	}
	
}