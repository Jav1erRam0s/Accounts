package utils;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import presentacion.controlador.ControladorVentanaPrincipal;
 
public class EncriptadorAES 
{

	private static EncriptadorAES instance;
	private SecretKeySpec secretKey;
	private Cipher cipher;
	
	public static EncriptadorAES getInstance()
	{
		if (instance == null)
			instance = new EncriptadorAES();
		return instance;
	}
	
	private EncriptadorAES() {
		String clave = ControladorVentanaPrincipal.contrasenia;
		
		try
		{
			byte[] claveEncriptacion = clave.getBytes("UTF-8");
	        MessageDigest sha = MessageDigest.getInstance("SHA-1");
	         
	        claveEncriptacion = sha.digest(claveEncriptacion);
	        claveEncriptacion = Arrays.copyOf(claveEncriptacion, 16);
	         
	        this.secretKey = new SecretKeySpec(claveEncriptacion, "AES");
	
	        this.cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	public void updateKey(String nuevaClave)
	{		
		try
		{
			byte[] claveEncriptacion = nuevaClave.getBytes("UTF-8");
	        MessageDigest sha = MessageDigest.getInstance("SHA-1");
	         
	        claveEncriptacion = sha.digest(claveEncriptacion);
	        claveEncriptacion = Arrays.copyOf(claveEncriptacion, 16);
	         
	        this.secretKey = new SecretKeySpec(claveEncriptacion, "AES");
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

    public String encriptar(String dato) throws Exception 
    {  
        this.cipher.init(Cipher.ENCRYPT_MODE, this.secretKey);
 
        byte[] datosEncriptar = dato.getBytes();
        byte[] bytesEncriptados = cipher.doFinal(datosEncriptar);
        return Base64.getEncoder().encodeToString(bytesEncriptados);
    }

    public String desencriptar(String dato) throws Exception 
    {
        this.cipher.init(Cipher.DECRYPT_MODE, this.secretKey);
         
        byte[] bytesEncriptados = Base64.getDecoder().decode(dato);
        byte[] datosDesencriptados = cipher.doFinal(bytesEncriptados);
        return new String(datosDesencriptados);
    }
    
}