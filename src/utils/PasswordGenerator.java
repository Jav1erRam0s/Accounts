package utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PasswordGenerator
{
	 
	public static String NUMEROS = "0123456789"; // 10 caracteres
	public static String MAYUSCULAS = "ABCDEFGHIJKLMN�OPQRSTUVWXYZ"; // 27 caracteres
	public static String MINUSCULAS = "abcdefghijklmn�opqrstuvwxyz"; // 27 caracteres
    public static String SIMBOLOS = "!|#$%&�?@~_.,:;'\"`<>()[]{}+-*/\\^="; // 33 caracteres
 
	public static String getPassword(int length)
	{
		int cantEspeciales = 2;
		String passAlfaNumericoEspeciales = "";
		
		String passAlfaNumerico = getPassword(NUMEROS + MAYUSCULAS + MINUSCULAS, length - cantEspeciales);
		String passEspeciales = getPassword(SIMBOLOS, cantEspeciales);
		
		List<String> values = Arrays.asList(passAlfaNumerico.concat(passEspeciales).split(""));

		Collections.shuffle(values);
		
	    for (String character : values) { passAlfaNumericoEspeciales = passAlfaNumericoEspeciales.concat(character); }
	    
		return passAlfaNumericoEspeciales;
	}
 
	private static String getPassword(String key, int length)
	{
		String pswd = "";
 
		for (int i = 0; i < length; i++) 
		{
			pswd+=(key.charAt((int)(Math.random() * key.length())));
		}
		return pswd;
	}
	
}