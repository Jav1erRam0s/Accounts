package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import dto.CuentaDTO;
import presentacion.controlador.ControladorVentanaPrincipal;

public class GeneradorDeReporte 
{
	
	private Document document;
	
	public GeneradorDeReporte(String ruta)
	{	
		this.document = new Document();
		
        try 
        {
        	PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(new File(ruta)));
    		
        	//----------------------------------------------------------------
            //ENCRIPTACION DEL DOCUMENTO
            //----------------------------------------------------------------
            
			writer.setEncryption(
					ControladorVentanaPrincipal.contrasenia.getBytes(), 
					ControladorVentanaPrincipal.contrasenia.getBytes(), 
					PdfWriter.ALLOW_PRINTING | PdfWriter.ALLOW_ASSEMBLY | PdfWriter.ALLOW_COPY | PdfWriter.ALLOW_DEGRADED_PRINTING | PdfWriter.ALLOW_FILL_IN | PdfWriter.ALLOW_MODIFY_ANNOTATIONS | PdfWriter.ALLOW_MODIFY_CONTENTS | PdfWriter.ALLOW_SCREENREADERS, 
					PdfWriter.ENCRYPTION_AES_256);
		} 
        catch (FileNotFoundException e) 
        {
			e.printStackTrace();
		} 
        catch (DocumentException e) 
        {
			e.printStackTrace();
		}
	}
	
	public void ReporteCuentas( List<CuentaDTO> cuentas )
	{
		//----------------------------------------------------------------
        //METADATOS
        //----------------------------------------------------------------
        
		this.document.addAuthor(System.getProperty("user.name"));
        this.document.addCreator("Accounts");
		this.document.addTitle("Accounts Report");
		this.document.addSubject("Accounts Report implemented with ITextPDF.");
		this.document.addCreationDate();
	    //this.document.addKeywords("N�, Aplicaci�n, Descripci�n, Usuario, Contrase�a");
        
		try 
		{
			//----------------------------------------------------------------
	        //CREAR TITULO
	        //----------------------------------------------------------------
            
            Image headerImg = Image.getInstance("src/resources/images/headerReport.png");
            headerImg.setAlignment(Element.ALIGN_CENTER);
            headerImg.scalePercent(22);
			document.add(headerImg);
            
 			//----------------------------------------------------------------
 	        //FECHA DE REPORTE
 	        //----------------------------------------------------------------

            Date fechaActual = new Date();
			String fechaActualString = new SimpleDateFormat("dd/MM/yyyy").format(fechaActual);
	        Font fuenteFecha = new Font();
	        fuenteFecha.setStyle(Font.BOLD);
	        fuenteFecha.setSize(10);
	        Paragraph p4 = new Paragraph(fechaActualString, fuenteFecha);
	        p4.setAlignment(Element.ALIGN_RIGHT);
	        p4.setSpacingAfter(22f);;
	        document.add(p4);
			
	        //----------------------------------------------------------------
	        //CREAR UNA TABLA
	        //----------------------------------------------------------------
	        
	        PdfPTable table = new PdfPTable(5);
	
	        table.setTotalWidth(new float[]{ 32, 100, 170, 125, 125 });
	        table.setLockedWidth(true);
	        
	        //Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);	        
	        Font boldFont = new Font();
	        boldFont.setStyle(Font.BOLD);
	        
	        PdfPCell c1 = new PdfPCell(new Phrase("N�", boldFont));
	        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        	c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
	        c1.setBackgroundColor(new BaseColor( 255, 154, 0 ));
        	c1.setPaddingTop(5);
        	c1.setPaddingBottom(5);
	        table.addCell(c1);
	        
	        c1 = new PdfPCell(new Phrase("Aplicaci�n", boldFont));
	        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        	c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
	        c1.setBackgroundColor(new BaseColor( 255, 154, 0 ));
        	c1.setPaddingTop(5);
        	c1.setPaddingBottom(5);
	        table.addCell(c1);
	
	        c1 = new PdfPCell(new Phrase("Descripci�n", boldFont));
	        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        	c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
	        c1.setBackgroundColor(new BaseColor( 255, 154, 0 ));
        	c1.setPaddingTop(5);
        	c1.setPaddingBottom(5);
	        table.addCell(c1);
	        
	        c1 = new PdfPCell(new Phrase("Usuario", boldFont));
	        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        	c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
	        c1.setBackgroundColor(new BaseColor( 255, 154, 0 ));
        	c1.setPaddingTop(5);
        	c1.setPaddingBottom(5);
	        table.addCell(c1);

	        c1 = new PdfPCell(new Phrase("Contrase�a", boldFont));
	        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        	c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
	        c1.setBackgroundColor(new BaseColor( 255, 154, 0 ));
        	c1.setPaddingTop(5);
        	c1.setPaddingBottom(5);
	        table.addCell(c1);
	    	
	        int contador = 1;
	        
	        List<CuentaDTO> listOrdenada = OrdenamientoDeCuentas.ordenar(cuentas);
	        
	        for (CuentaDTO cuenta : listOrdenada)
	        {
		        c1 = new PdfPCell(new Phrase(Integer.toString(contador), boldFont));
	 	        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
	        	c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		        c1.setBackgroundColor(new BaseColor( 245, 245, 245 ));
	        	c1.setPaddingTop(5);
	        	c1.setPaddingBottom(5);
	        	table.addCell(c1);
	        	try 
	        	{
					Paragraph parrafo = new Paragraph();
					Font fuenteEnlace = new Font();
					fuenteEnlace.setColor( BaseColor.BLUE);
					fuenteEnlace.setStyle(Font.NORMAL);  
					Anchor enlace = new Anchor(cuenta.getApp(), fuenteEnlace);
		            enlace.setReference(cuenta.getUrl());
		            parrafo.add(enlace);
		            c1 = new PdfPCell(parrafo);
		 	        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		        	c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			        c1.setBackgroundColor(new BaseColor( 245, 245, 245 ));
		        	c1.setPaddingTop(5);
		        	c1.setPaddingBottom(5);
		        	c1.setPaddingLeft(7);
		        	c1.setPaddingRight(7);
		 	        table.addCell( c1 );
		        	
		        	c1 = new PdfPCell(new Phrase(cuenta.getDescription()));
		        	c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			        c1.setBackgroundColor(new BaseColor( 245, 245, 245 ));
		        	c1.setPaddingTop(5);
		        	c1.setPaddingBottom(5);
		        	c1.setPaddingLeft(7);
		        	c1.setPaddingRight(7);
		 	        table.addCell( c1 );
		            	        		
		        	c1 = new PdfPCell(new Phrase(cuenta.getUser()));
		        	c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		        	c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			        c1.setBackgroundColor(new BaseColor( 245, 245, 245 ));
		        	c1.setPaddingTop(5);
		        	c1.setPaddingBottom(5);
		        	c1.setPaddingLeft(7);
		        	c1.setPaddingRight(7);
					table.addCell( c1 );
					
		        	c1 = new PdfPCell(new Phrase(cuenta.getPass()));
					c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		        	c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			        c1.setBackgroundColor(new BaseColor( 245, 245, 245 ));
		        	c1.setPaddingTop(5);
		        	c1.setPaddingBottom(5);
		        	c1.setPaddingLeft(7);
		        	c1.setPaddingRight(7);
					table.addCell( c1 );
	        	} 
	        	catch (Exception e) 
	        	{
					e.printStackTrace();
				}
	        	contador++;
	        }
	        
	        document.add(table);    
		} 
		catch (DocumentException | IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void openDocument()
	{
		this.document.open();
	}
	
	public void closeDocument()
	{
		this.document.close();
	}
	
}