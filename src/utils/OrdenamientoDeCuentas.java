package utils;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import dto.CuentaDTO;

public class OrdenamientoDeCuentas 
{

	public static List<CuentaDTO> ordenar(List<CuentaDTO> cuentas) 
	{
		//aplico el ordenamiento por nombre de app.
		Comparator<CuentaDTO> comparador =  new Comparator<CuentaDTO>() {
            public int compare( CuentaDTO a, CuentaDTO b ) {
                int	resultado = a.getApp().compareTo(b.getApp());
                return resultado;
            }
        };
        Collections.sort(cuentas,  comparador );
		return cuentas;
	}
	
}