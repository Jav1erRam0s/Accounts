package presentacion.controlador;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import dto.CuentaDTO;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import modelo.GestorCuentas;
import utils.GeneradorDeReporte;
import utils.ImportExportBD;
import utils.NotificationsPrincipal;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;

public class ControladorVentanaPrincipal implements ClipboardOwner
{
	@FXML
	public BorderPane borderPaneVentanaPrincipal;

	@FXML
	public Button btnBuscador;
	@FXML
	public Button btnAgregar;
	@FXML
	public Button btnEditar;
	@FXML
	public Button btnEliminar;
	@FXML
	public Button btnDescargar;
	@FXML
	public Button btnConfigurar;

	@FXML
	public TableView<CuentaDTO> tblCuentas;
	@FXML
	private TableColumn<CuentaDTO, String> colAplicacion;
	@FXML
	private TableColumn<CuentaDTO, String> colDescripcion;
	@FXML
	private TableColumn<CuentaDTO, String> colUsuario;
	@FXML
	private TableColumn<CuentaDTO, String> colContrasenia;
	
	// NOTIFICACION
	@FXML
	public HBox hboxNotification;
	@FXML
	public Label lblNoticationImage;
	@FXML
	public Label lblNoticationMessage;
	
	
	public Tooltip tooltip = new Tooltip();
	
	@FXML
	public TextField txtFiltro;

	@FXML
	public MenuButton MenuButtonBD;
	
	public ObservableList<CuentaDTO> cuentas;
	public ObservableList<CuentaDTO> cuentasFiltradas;

	@FXML
	public HBox hBoxBuscador;
	@FXML
	public HBox hBoxActions;

	public static String contrasenia;
	
	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
	
	@SuppressWarnings({ "rawtypes", "deprecation" })
	public void initialize(String contrasenia)
	{	
		ControladorVentanaPrincipal.contrasenia = contrasenia;
		
		this.txtFiltro.requestFocus();

		Image img0 = new Image("resources/images/buscador.png");
	    ImageView view0 = new ImageView(img0);
	    view0.setFitHeight(17);
	    view0.setFitWidth(17);
	    this.btnBuscador.setGraphic(view0);
	    
	    Image img2 = new Image("resources/images/agregar.png");
	    ImageView view2 = new ImageView(img2);
	    view2.setFitHeight(17);
	    view2.setFitWidth(17);
	    this.btnAgregar.setGraphic(view2);
	    
	    Image img3 = new Image("resources/images/editar.png");
	    ImageView view3 = new ImageView(img3);
	    view3.setFitHeight(17);
	    view3.setFitWidth(17);
	    this.btnEditar.setGraphic(view3);
	    
	    Image img4 = new Image("resources/images/eliminar.png");
	    ImageView view4 = new ImageView(img4);
	    view4.setFitHeight(17);
	    view4.setFitWidth(17);
	    this.btnEliminar.setGraphic(view4);

		Image img5 = new Image("resources/images/bd.png");
	    ImageView view5 = new ImageView(img5);
	    view5.setFitHeight(17);
	    view5.setFitWidth(17);
		this.MenuButtonBD.setGraphic(view5);

	    Image img6 = new Image("resources/images/descargar.png");
	    ImageView view6 = new ImageView(img6);
	    view6.setFitHeight(17);
	    view6.setFitWidth(17);
	    this.btnDescargar.setGraphic(view6);
	    
	    Image img7 = new Image("resources/images/configurar.png");
	    ImageView view7 = new ImageView(img7);
	    view7.setFitHeight(17);
	    view7.setFitWidth(17);
	    this.btnConfigurar.setGraphic(view7);
		
		this.txtFiltro.setStyle("-fx-text-inner-color: #0096C9; -fx-background-color: black;");
		
	    this.borderPaneVentanaPrincipal.setStyle("-fx-background-image: url('resources/images/fondoPrincipal.jpg'); -fx-background-size: cover;");
		this.tooltip.setStyle("-fx-text-fill: #FF9A00;");	    
		
		this.colAplicacion.prefWidthProperty().bind( this.tblCuentas.widthProperty().multiply(.199) );
		this.colDescripcion.prefWidthProperty().bind( this.tblCuentas.widthProperty().multiply(.35) );
		this.colUsuario.prefWidthProperty().bind( this.tblCuentas.widthProperty().multiply(.25) );
		this.colContrasenia.prefWidthProperty().bind( this.tblCuentas.widthProperty().multiply(.18) );
		
		this.colAplicacion.setResizable(false);
		this.colDescripcion.setResizable(false);
		this.colUsuario.setResizable(false);
		this.colContrasenia.setResizable(false);

		// Deshabilitar el reordenamiento de las columnas.
		this.colAplicacion.impl_setReorderable(false);
		this.colDescripcion.impl_setReorderable(false);
		this.colUsuario.impl_setReorderable(false);
		this.colContrasenia.impl_setReorderable(false);
		
		this.btnEditar.setDisable(true);
		this.btnEliminar.setDisable(true);

	    this.hBoxBuscador.getStylesheets().add("styles/panelControlPrincipal.css");
	    this.hBoxActions.getStylesheets().add("styles/panelControlPrincipal.css");
	    
		this.cuentas = FXCollections.observableArrayList( GestorCuentas.getInstance().readAll() );
		this.cuentasFiltradas = FXCollections.observableArrayList( GestorCuentas.getInstance().readAll() );
		this.tblCuentas.setItems(this.cuentas);

		this.tblCuentas.getStylesheets().add("styles/tablaPrincipal.css");
				
		this.colAplicacion.setCellValueFactory(new PropertyValueFactory<>("app"));
		this.colDescripcion.setCellValueFactory(new PropertyValueFactory<>("description"));
		this.colUsuario.setCellValueFactory(new PropertyValueFactory<>("user"));
		this.colContrasenia.setCellValueFactory(new PropertyValueFactory<>("pass"));
		
		this.colAplicacion.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");
		this.colDescripcion.setStyle(" -fx-text-fill : #000000; ");
		this.colUsuario.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");
		this.colContrasenia.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");
		
		// Esto es para que el scroll de la tabla se vaya hasta abajo.
		//Platform.runLater( () -> this.tblCuentas.scrollTo(this.cuentas.size()-1) );
		
		Image imgE = new Image("resources/images/export.png");
		ImageView imgExport = new ImageView(imgE);
		imgExport.setFitWidth(10);
		imgExport.setFitHeight(10);

		Image imgI = new Image("resources/images/import.png");
		ImageView imgImport = new ImageView(imgI);
		imgImport.setFitWidth(10);
		imgImport.setFitHeight(10);
		
		MenuItem menuItemExport = new MenuItem("Exportar", imgExport);
		MenuItem menuItemImport = new MenuItem("Importar", imgImport);
		
		menuItemExport.setOnAction(e -> this.exportar());
		menuItemImport.setOnAction(e -> this.importar());

		this.MenuButtonBD.getItems().addAll( menuItemExport, menuItemImport );
		
//		this.tblCuentas.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		this.tblCuentas.getFocusModel().focusedCellProperty().addListener(
		    new ChangeListener<TablePosition>() 
		{
		    @Override
		    public void changed(ObservableValue<? extends TablePosition> observable,
		            TablePosition oldPos, TablePosition pos) 
		    {
		        int row = pos.getRow();
		        int column = pos.getColumn();
		        String selectedValue = "";

		        if (tblCuentas.getItems().size() > row && column >=0 && column <= 3 ) 
		        {
		        	if (column==0) { selectedValue = tblCuentas.getItems().get(row).getApp(); }
		        	else if (column==1) { selectedValue = tblCuentas.getItems().get(row).getDescription(); }
		        	else if (column==2){ selectedValue = tblCuentas.getItems().get(row).getUser(); }
		        	else if (column==3){ selectedValue = tblCuentas.getItems().get(row).getPass(); }
			    
		        	setClipboard(selectedValue);
		        }
		    }
		});
		
		hboxNotification.setVisible(false);
	}

	public void setClipboard(String value)
	{
        StringSelection txt = new StringSelection(value);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(txt, this);
	}
	
	@Override
	public void lostOwnership(Clipboard arg0, Transferable arg1) { }

	//**********************************************************

	@FXML
	public void tooltipBuscador(MouseEvent event)
	{
		if (event.getSource() == this.btnBuscador)
		{
			this.tooltip.setText("Limpiar buscador");
			ImageView view = new ImageView("resources/images/limpiar.png");
			view.setFitHeight(20);
		    view.setFitWidth(20);
			this.tooltip.setGraphic(view);
			this.btnBuscador.setTooltip(tooltip);
		}
	}
	
	@FXML
	public void limpiarBuscador(ActionEvent event) 
	{
		this.txtFiltro.clear();
		this.txtFiltro.requestFocus();
		this.filtrar(null);	
	}

	@FXML
	public void filtrar(KeyEvent event) 
	{
		this.btnEditar.setDisable(true);
		this.btnEliminar.setDisable(true);
		
		String nombre = this.txtFiltro.getText().toLowerCase();
		this.tooltip.setText(null);
		
		if( nombre.isEmpty() )
		{
			this.cuentasFiltradas.clear();
			this.cuentasFiltradas = FXCollections.observableArrayList( GestorCuentas.getInstance().readAll() );
			
			this.tblCuentas.setItems(this.cuentasFiltradas);
			this.tblCuentas.refresh();
		}
		else
		{
			this.cuentasFiltradas.clear();
			for(CuentaDTO p : this.cuentas)
			{
				String name = p.getApp();
				String description = p.getDescription();
				String cadena = name + " " + description;
				if( cadena.toLowerCase().contains(nombre) )
				{
					this.cuentasFiltradas.add(p);
				}
			}
			this.tblCuentas.setItems(this.cuentasFiltradas);	
			this.tblCuentas.refresh();
		}
	}
		
	@FXML
	public void agregarCuenta(ActionEvent event) 
	{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/presentacion/vista/VentanaAgregarEditarCuenta.fxml"));
		
		try 
		{
			Parent root = loader.load();
			
			ControladorVentanaAgregarEditarCuenta contro = loader.getController();
			contro.initialize("agregar", null, this);

			this.borderPaneVentanaPrincipal.setLeft(root);			
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	@FXML
	public void editarCuenta(ActionEvent event) 
	{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/presentacion/vista/VentanaAgregarEditarCuenta.fxml"));
		
		try 
		{
			Parent root = loader.load();
			
			ControladorVentanaAgregarEditarCuenta contro = loader.getController();
			contro.initialize("editar", this.tblCuentas.getSelectionModel().getSelectedItem(), this);

			this.borderPaneVentanaPrincipal.setLeft(root);			
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}	
	}
	
	@FXML
	public void eliminarCuenta(ActionEvent event) 
	{
		CuentaDTO cuentaSeleccionada = this.tblCuentas.getSelectionModel().getSelectedItem();
		
		this.deshabilitarPantallaPrincipal();
		
		boolean result = this.mostrarConfirmacionEliminar(null, cuentaSeleccionada).get() == ButtonType.YES;
	    
		if (result)
	    {
			GestorCuentas.getInstance().delete(cuentaSeleccionada);

			NotificationsPrincipal on = new NotificationsPrincipal(this, "eliminado", "Eliminado");
			on.start();
	    }
	    else {
	    	this.habilitarPantallaPrincipal();
	    }
	}

	private Optional<ButtonType> mostrarConfirmacionEliminar(ActionEvent event, CuentaDTO cuenta)
	{
	    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
	    alert.setHeaderText(null);
	    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("/resources/images/accounts.png"));
        
        // Create the ImageView we want to use for the icon
        Image imgConfirmation = new Image("resources/images/borrar.png");
        ImageView icon = new ImageView(imgConfirmation);
        // The standard Alert icon size is 48x48, so let's resize our icon to match
        icon.setFitHeight(48);
        icon.setFitWidth(48);
        // Set our new ImageView as the alert's icon
        alert.getDialogPane().setGraphic(icon);
        
	    alert.setTitle("Confirmación");
	    alert.setContentText("¿Deseas realmente eliminar '" + cuenta.getApp() + "'?");
	    
	    alert.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
	    //Deactivate Defaultbehavior for yes-Button:
	    Button yesButton = (Button) alert.getDialogPane().lookupButton( ButtonType.YES );
	    yesButton.setDefaultButton( false );
	    //Activate Defaultbehavior for no-Button:
	    Button noButton = (Button) alert.getDialogPane().lookupButton( ButtonType.NO );
	    noButton.setDefaultButton( true );
	    
	    return alert.showAndWait();
	}

    public void exportar() 
	{
		this.deshabilitarPantallaPrincipal();
		
		String user = System.getProperty("user.name");
		String rutaExport = "C:/Users/"+user+"/Downloads";
		
        if( ImportExportBD.Exportar( rutaExport ) )
        {
			NotificationsPrincipal on = new NotificationsPrincipal(this, "exportado", "Exportación realizada");
			on.start();
        }
        else
        {
        	NotificationsPrincipal on = new NotificationsPrincipal(this, "error", "Error de exportación");
			on.start();
        }
	}

    public void importar() 
	{
    	this.deshabilitarPantallaPrincipal();
    	
		FileChooser select = new FileChooser();
		select.setTitle("Importar");
		
		String user = System.getProperty("user.name");
		File defaultDirectory = new File("C:/Users/"+user+"/Downloads");
		select.setInitialDirectory(defaultDirectory);
		select.getExtensionFilters().addAll( new FileChooser.ExtensionFilter("SQL", "*.sql") );
		
		Stage newStage = new Stage();
		
		File file = select.showOpenDialog( newStage );

        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        newStage.setX((screenBounds.getWidth() - newStage.getWidth()) / 2);
        newStage.setY((screenBounds.getHeight() - newStage.getHeight()) / 2);

		if (file != null) 
		{
            String fileAsString = file.toString();
            if( ImportExportBD.Importar( fileAsString ) )
            {
            	NotificationsPrincipal on = new NotificationsPrincipal(this, "importado", "Importación realizada");
    			on.start();
            }
            else
            {
    			NotificationsPrincipal on = new NotificationsPrincipal(this, "error", "Error de importación");
    			on.start();
            }
        }
		else
		{
			this.habilitarPantallaPrincipal();
		}
	}
    
	@FXML
	public void crearReporte(ActionEvent event) 
	{	
		this.deshabilitarPantallaPrincipal();
		
		String user = System.getProperty("user.name");
        String rutaReport = "C:/Users/"+user+"/Downloads/AccountsReport_"+dtf.format(LocalDateTime.now())+".pdf";
        
        GeneradorDeReporte gen = new GeneradorDeReporte(rutaReport);

    	try
		{
			gen.openDocument();
			gen.ReporteCuentas(GestorCuentas.getInstance().readAll());
			gen.closeDocument();
			
			NotificationsPrincipal on = new NotificationsPrincipal(this, "reportado", "Reporte creado");
			on.start();
		}
		catch(Exception ex)
		{
			NotificationsPrincipal on = new NotificationsPrincipal(this, "error", "Error al crear el reporte");
			on.start();
		}
	}

	@FXML
	public void cambiarContrasenia(ActionEvent event) 
	{
		this.deshabilitarPantallaPrincipal();
		
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/presentacion/vista/VentanaCambiarPassword.fxml"));
		
		try 
		{
			//hacemos una referencia para cargar al padre.
			Parent root = loader.load();
			
			//Scene scene = new Scene(root, 230, 200);
			Scene scene = new Scene(root, 220, 220);
			
			Stage stage = new Stage();
			stage.setTitle("Accounts");
			stage.getIcons().add(new Image("/resources/images/configurar.png"));
			
			ControladorVentanaCambiarPassword contro = loader.getController();
			contro.initialize(this);

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.setResizable(false);
			stage.show();
			
			// Habilitamos los components al cerrar VentanaCambiarPassword.
			stage.setOnCloseRequest( habilitarPantallaPrincipal -> { habilitarPantallaPrincipal(); } );
			
	        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
	        stage.setX((screenBounds.getWidth() - stage.getWidth()) / 2);
	        stage.setY((screenBounds.getHeight() - stage.getHeight()) / 2);
		} 
		catch (IOException e) 
		{ e.printStackTrace(); }
	}
	
	public void deshabilitarPantallaPrincipal()
	{
		this.hBoxBuscador.setStyle("-fx-border-radius: 5; -fx-border-width: 2; -fx-border-style:solid; -fx-border-color: #004d6755; -fx-background-color: black; -fx-background-radius: 8");
		this.hBoxActions.setStyle("-fx-border-radius: 5; -fx-border-width: 2; -fx-border-style:solid; -fx-border-color: #4d565655; -fx-background-color: black; -fx-background-radius: 8");
		
		this.txtFiltro.setDisable(true);
		this.txtFiltro.setText("");
		this.txtFiltro.requestFocus();
		this.filtrar(null);
		
		this.btnAgregar.setDisable(true);
		this.btnEditar.setDisable(true);
		this.btnEliminar.setDisable(true);
		this.MenuButtonBD.setDisable(true);
		this.btnDescargar.setDisable(true);
		this.btnConfigurar.setDisable(true);
		this.btnBuscador.setDisable(true);

		this.tblCuentas.setDisable(true);
	}
	
	public void habilitarPantallaPrincipal()
	{
	    this.hBoxBuscador.setStyle("-fx-border-radius: 5; -fx-border-width: 2; -fx-border-style:solid; -fx-border-color: #004d67; -fx-background-color: black; -fx-background-radius: 8");
	    this.hBoxActions.setStyle("-fx-border-radius: 5; -fx-border-width: 2; -fx-border-style:solid; -fx-border-color: #4d5656; -fx-background-color: black; -fx-background-radius: 8");
		
	    this.txtFiltro.setDisable(false);
		this.txtFiltro.setText("");
		this.txtFiltro.requestFocus();
		//this.filtrar(null);
		
		this.btnAgregar.setDisable(false);
		this.MenuButtonBD.setDisable(false);
		this.btnDescargar.setDisable(false);
		this.btnConfigurar.setDisable(false);
		this.btnBuscador.setDisable(false);
		
		this.tblCuentas.setDisable(false);
	}

	//**********************************************************
	
	@FXML
	public void openUrlMouseAndEnableActions(MouseEvent event) 
	{
		//Si doble click -> Abrir enlace en el browser
		if( event.getClickCount()==2 && this.tblCuentas.getSelectionModel().getSelectedIndex() != -1 )
		{
			this.openUrl();
		}
		
		//Habilitar acciones
		if( this.tblCuentas.getSelectionModel().getSelectedIndex() != -1 )
		{
			this.borderPaneVentanaPrincipal.setLeft(null);
			this.btnAgregar.setDisable(false);
			this.btnEditar.setDisable(false);
			this.btnEliminar.setDisable(false);
			this.MenuButtonBD.setDisable(false);
			this.btnDescargar.setDisable(false);
			this.btnConfigurar.setDisable(false);
		}
		else
		{
			this.btnEditar.setDisable(true);
			this.btnEliminar.setDisable(true);
		}
	}

	@FXML	
	public void tooltipTableKeyAndOpenUrlEnter(KeyEvent event) 
	{
		//Si Enter -> Abrir enlace en el browser
		if (event.getCode() == KeyCode.ENTER)
		{
			if( this.tblCuentas.getSelectionModel().getSelectedIndex() != -1 )
			{
				this.openUrl();
			}
		}
		//Mostrar tooltips
		this.tooltipTable();
	}

	@FXML
	public void tooltipTableMouse(MouseEvent event) 
	{
		this.tooltipTable();
	}
	
	private void tooltipTable()
	{
		if( this.tblCuentas.getSelectionModel().getSelectedIndex() != -1 )
		{
			String mensaje = "";
			mensaje = mensaje + this.tblCuentas.getSelectionModel().getSelectedItem().getApp();
			if(	this.tblCuentas.getSelectionModel().getSelectedItem().getDescription().length() != 0 )
			{ mensaje = mensaje  + "\n" + "---" + "\n" + this.tblCuentas.getSelectionModel().getSelectedItem().getDescription(); }
			if( mensaje.length() == 0) 
			{
				this.tooltip.setGraphic(null);
				this.tooltip.setText("Empty");
			}
			else 
			{ 
				ImageView view = new ImageView("resources/images/app.png");
				view.setFitHeight(25);
			    view.setFitWidth(25);
				this.tooltip.setGraphic(view);
				this.tooltip.setText(mensaje);
				//this.tooltip.setTextAlignment(TextAlignment.CENTER);
			}
			this.tblCuentas.setTooltip(tooltip);
		}
		else
		{
			this.tblCuentas.setTooltip(null);
		}
	}

	private void openUrl(){
		if(this.tblCuentas.getSelectionModel().getSelectedItem().getUrl().isEmpty() == false)
    	{
			Desktop desktop = Desktop.getDesktop();
            if(desktop.isSupported(Desktop.Action.BROWSE)) 
            {
            	try 
                {   desktop.browse(new URI(this.tblCuentas.getSelectionModel().getSelectedItem().getUrl().toString()));   }
                catch (Exception ex)
                {	
            		this.deshabilitarPantallaPrincipal();
        			NotificationsPrincipal on = new NotificationsPrincipal(this, "error", "No es un enlace valido");
        			on.start();
                }            		
            }
    	}
        else
    	{
    		this.deshabilitarPantallaPrincipal();
			NotificationsPrincipal on = new NotificationsPrincipal(this, "error", "No es un enlace valido");
			on.start();            		
    	}
	}

	//**********************************************************
	
}