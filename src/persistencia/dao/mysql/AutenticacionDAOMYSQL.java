package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dto.AutenticacionDTO;
import persistencia.dao.interfaz.AutenticacionDAO;
import persistencia.conexion.Conexion;

public class AutenticacionDAOMYSQL implements AutenticacionDAO
{
	
	private static AutenticacionDAOMYSQL instance;
	private static final String update = "UPDATE autenticacion SET hash=? WHERE idHash=?";
	private static final String readForId = "SELECT * FROM autenticacion WHERE idHash=?";
	
	public static AutenticacionDAOMYSQL getInstance()
	{
		if (instance == null)
			instance = new AutenticacionDAOMYSQL();
		return instance;
	}
	

	@Override
	public boolean update(AutenticacionDTO authen) 
	{
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(update);
			statement.setString(1, authen.getHash());
			statement.setInt(2, authen.getIdHash());
			if(statement.executeUpdate() > 0)
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public AutenticacionDTO readForId(int idHash) 
	{
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		AutenticacionDTO authen = new AutenticacionDTO(); 
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readForId);
			statement.setInt(1, idHash);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				authen.setIdHash(resultSet.getInt("idHash"));
				authen.setHash(resultSet.getString("hash"));
			}	
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return authen;
	}
	
}