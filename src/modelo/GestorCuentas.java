package modelo;

import java.util.List;
import dto.CuentaDTO;
import persistencia.dao.mysql.CuentaDAOMYSQL;

public class GestorCuentas 
{
	
	private static GestorCuentas instance;
	private CuentaDAOMYSQL cuentaDaoSQL;
	
	private GestorCuentas() 
	{
		this.cuentaDaoSQL = CuentaDAOMYSQL.getInstance();
	}

	public static GestorCuentas getInstance() 
	{
		if ( instance == null )
			instance = new GestorCuentas();
		return instance;
	}

	public void insert(CuentaDTO cuenta) 
	{
		this.cuentaDaoSQL.insert(cuenta);
	}
	
	public void delete(CuentaDTO cuenta)
	{
		this.cuentaDaoSQL.delete(cuenta);
	}
	
	public void update(CuentaDTO cuenta)
	{
		this.cuentaDaoSQL.update(cuenta);
	}

	public List<CuentaDTO> readAll() 
	{
		return this.cuentaDaoSQL.readAll();
	}
	
	public CuentaDTO readForId(String idAccount)
	{
		return this.cuentaDaoSQL.readForId(idAccount);
	}
	
}